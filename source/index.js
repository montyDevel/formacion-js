
import { PlayersList } from './more-javascript/PlayersList.js';
import { TeamsGenerator } from './more-javascript/TeamsGenerator.js';
import './style/style.scss';

new PlayersList(document.getElementById('players-list'));
new TeamsGenerator(document.getElementById('teams'));


