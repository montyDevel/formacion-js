
export function playMatch(dataObject) {
    return new Promise((resolve)=>{
        let {teamA, teamB} = dataObject.data;
        let teamAName = 'Equipo A';
        let teamBName = 'Equipo B';
        let winnerTeam, looserTeam, winnerTeamName, looserTeamName;
        let winner = Math.round(Math.random());

        if (winner){
            winnerTeam = teamA; 
            looserTeam = teamB;
            winnerTeamName = teamAName;
            looserTeamName = teamBName;
        }else{
            winnerTeam = teamB; 
            looserTeam = teamA;
            winnerTeamName = teamBName;
            looserTeamName = teamAName;
        }
        let resultMsg = `${looserTeam[0]} le ha pegado tan fuerte a la pelota que ha saltado del futbolín y le ha dado a ${winnerTeam[0]} en un ojo. De la risa ${looserTeam[1]} que estaba de portero en el equipo contrario no se ha dado cuenta que la pelota ha rebotado y ha entrado en la porteria. Gana el equipo ${winnerTeamName}`;
        setTimeout(()=>{
            resolve(resultMsg);
        }, 3000);

    });
}
